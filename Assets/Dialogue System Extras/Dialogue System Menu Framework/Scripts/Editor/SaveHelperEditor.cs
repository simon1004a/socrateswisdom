﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace PixelCrushers.DialogueSystem.MenuSystem
{

    [CustomEditor(typeof(SaveHelper))]
    public class SaveHelperEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button(new GUIContent("Delete Saved Games", "Delete all saved games.")))
            {
                if (EditorUtility.DisplayDialog("Delete Saved Games", "Delete all saved games?", "OK", "Cancel"))
                {
                    DeleteSavedGames();
                }
            }
        }

        private void DeleteSavedGames()
        {
            var saveHelper = target as SaveHelper;
            if (saveHelper == null) return;
            for (int i = 0; i < 100; i++)
            {
                PlayerPrefs.DeleteKey("savedgame_" + i + "_summary");
                PlayerPrefs.DeleteKey("savedgame_" + i + "_details");
                PlayerPrefs.DeleteKey("savedgame_" + i + "_data");
            }

            // Delete saved games on disk:
            if (saveHelper.GetComponent<SaveToDisk>() != null)
            {
                var saveInfoFilename = Application.persistentDataPath + "/saveinfo.dat";
                if (File.Exists(saveInfoFilename))
                {
                    File.Delete(saveInfoFilename);
                }
            }
        }
    }
}