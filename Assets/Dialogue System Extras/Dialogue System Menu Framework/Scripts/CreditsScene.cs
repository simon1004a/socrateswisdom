﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace PixelCrushers.DialogueSystem.MenuSystem
{

    /// <summary>
    /// This script handles the credits scene. It just provides a method to
    /// return to the title menu scene.
    /// </summary>
    public class CreditsScene : MonoBehaviour
    {
        public Text philosophyText;
        public Text mathText;

        //Credit 을 보여주도로 짜긴 하였으나 아직 LastSavedGame 데이터를 로드하기 전이라서
        //이 상태로 그냥 실행하면 0을 보여준다. 따라서 아직은 쓸 수 없는 코드이다.
        //그래서 18.9.7일 현재는 그냥 Credit 씬 버튼을 비활성화해놓았다. 
        private void Start()
        {
            int PhilosophyPoint = DialogueLua.GetVariable("PhilosophyPoint").AsInt;
            int MathPoint = DialogueLua.GetVariable("MathPoint").AsInt;

            philosophyText.text = "Philosophy : " + PhilosophyPoint.ToString();
            mathText.text = "Geometry : " + MathPoint.ToString();
        }

        public void ReturnToTitleScene()
        {
            var titleMenu = FindObjectOfType<TitleMenu>();
            SceneManager.LoadScene(titleMenu.titleSceneIndex);
            FindObjectOfType<TitleMenu>().titleMenuPanel.Open();
        }


    }
}
