﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    //페이스북 관련 데이터
    public string FacebookID;
    public string FacebookName;
    public string FacebookPhotoURL;
    public string FacebookAccessToken;

    //유저 데이터
    public long UserID;
    public string UserAccessToken;
    public System.DateTime CreatedAT;
    public string UserName;
    public string UserNation;
    

    //유저 레벨 정보
    public int UserLevel;
    public int PhilosophyCredit;
    public int MathCredit;
    public string CurrentPhilosophyQuest;
    public string CurrentMathQuest;
    public bool HasSolution;


    //처음에 생성할 때 다 빠지더라도 id랑 이름은 넣도록 하자. 
    public GameData(long id, string name)
    {
        this.FacebookID = "";
        this.FacebookName = "";
        this.FacebookPhotoURL = "";
        this.FacebookAccessToken = "";

        this.UserID = id;
        this.UserAccessToken = "";
        this.CreatedAT = System.DateTime.Now;
        this.UserName = name;
        this.UserNation = "";

        this.UserLevel = 0;
        this.PhilosophyCredit = 0;
        this.MathCredit = 0;
        this.CurrentPhilosophyQuest = "";
        this.CurrentMathQuest = "";
        this.HasSolution = false;
    }



}
