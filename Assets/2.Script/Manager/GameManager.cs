﻿using PixelCrushers.DialogueSystem;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {


    public static GameManager instance = null;


    //모드에 따라서 카메라 효과와 사운드 효과를 여기서 관리하자!!!
    public enum GameMode
    {
        PLAY_MODE,
        CONVERSATION_MODE,
        MATH_MODE,
        MINIMAP_MODE,
    }

    public GameMode PreviousMode = GameMode.PLAY_MODE;
    public GameMode CurrentMode = GameMode.PLAY_MODE;

    //카메라 변수
    public CameraCtrl m_CamearaCtrl;
    [HideInInspector]
    public bool bCamZoomMode = false;

    
    public Transform trPlayer, trConversant, trNextTarget;
    public Transform GetPlayerTransform() { return trPlayer; }
    public Transform GetConversantTransform() { return trConversant; }
    
    //사운드 관리
    public AudioSource efxSource;

    //문제 풀이에 따라 울리는 효과음들
    public AudioClip winnerClip1;
    public AudioClip winnerClip2;
    public AudioClip loserClip1;
    public AudioClip loserClip2;
        
    public float lowPitchRange = .95f;
    public float highPitchRange = 1.05f;


    //Credit 관리하기
    public Text PhilosophyCredit;
    public Text MathCredit;

    private static int PhilosophyPoint = 0;
    private static int MathPoint = 0;

    //모바일 조이스틱 컨트롤 관리하기
    public GameObject joyStick;
    public GameObject UICtrls;

    //미니맵 키고 끄기    
    public GameObject minimapSet;


    void Awake()
    {        
        //이 GameManager도 singleton으로 해야 한다!!
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

    }

    // called first
    void OnEnable()
    {        
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        
        if (scene.name == "GamePlay")
        {
            Debug.Log("OnSceneLoaded and GamePlay Scene reallocated assets.");
            trPlayer = GameObject.FindGameObjectWithTag("Player").transform;
            m_CamearaCtrl = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraCtrl>();
            m_CamearaCtrl.SetCameraTarget(trPlayer);
                        
            minimapSet = GameObject.FindGameObjectWithTag("Map");
            this.SetActiveMinimap(false);

            UICtrls.SetActive(true);
            UpdatePoint();
        }
    }

    // called third
    void Start()
    {
        //Debug.Log("Start");
    }

    // called when the game is terminated
    void OnDisable()
    {        
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    public void PlaySingle(AudioClip clip)
    {        
        efxSource.clip = clip;
        efxSource.Play();
    }

    public void PlaySuccess()
    {
        RandomizeSfx(winnerClip1, winnerClip2);
    }

    public void PlayLoss()
    {
        RandomizeSfx(loserClip1, loserClip2);
    }

    public void RandomizeSfx(params AudioClip[] clips)
    {
        int randomIndex = Random.Range(0, clips.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);
        efxSource.pitch = randomPitch;
        efxSource.clip = clips[randomIndex];
        efxSource.Play();
    }
    
    public IEnumerator ChangeGameModeLater(GameMode mode, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        ChangeGameMode(mode);
    }

    public void ChangeGameMode(GameMode mode)
    {
        if (mode == GameMode.CONVERSATION_MODE && trConversant == null)
        {
            Debug.Log("Err : There is no conversant, so can not convert to CONVERSATION_MODE");
            return;
        }

        PreviousMode = CurrentMode;
        CurrentMode = mode;

        Vector3 target;
        switch (CurrentMode)
        {
            case GameMode.CONVERSATION_MODE:
                minimapSet.SetActive(false);
                target = new Vector3(trConversant.position.x, trPlayer.position.y, trConversant.position.z);
                trPlayer.LookAt(target);
                m_CamearaCtrl.SetCameraTarget(GetConversationViewPoint(), trPlayer.right);
                joyStick.GetComponent<VirtualJoystick>().ResetMovement();
                UICtrls.SetActive(false);
                break;

            case GameMode.MATH_MODE:
                target = new Vector3(trConversant.position.x, trPlayer.position.y, trConversant.position.z);
                trPlayer.LookAt(target);
                m_CamearaCtrl.SetCameraTarget(GetConversationViewPoint(), trPlayer.right);
                joyStick.GetComponent<VirtualJoystick>().ResetMovement();
                UICtrls.SetActive(false);
                break;
            
            case GameMode.PLAY_MODE:
                UICtrls.SetActive(true);
                joyStick.GetComponent<VirtualJoystick>().ResetMovement();
                break;
        }

        Debug.Log("GameMode Changed to " + mode);

    }

    /*Player에 달린 Dialogue System Events Component에 달린 
    OnConversationStart Event가 실행될 때 이 함수가 불린다.*/
    public void StartConversation()
    {
        Debug.Log("Conversation Started in GameManager!!!");
        trConversant = DialogueManager.Instance.CurrentActor;
        if (trConversant == trPlayer)
            trConversant = DialogueManager.Instance.CurrentConversant;

        bCamZoomMode = m_CamearaCtrl.IsZoomIn;
        m_CamearaCtrl.IsZoomIn = true;

        ChangeGameMode(GameMode.CONVERSATION_MODE);
    }

    public void EndConversation()
    {
        if(CurrentMode != GameMode.MATH_MODE)
        {            
            m_CamearaCtrl.IsZoomIn = bCamZoomMode;
            ChangeGameMode(GameMode.PLAY_MODE);
        }

        UpdatePoint();
        
    }

    
    public void UpdatePoint()
    {
        PhilosophyPoint = DialogueLua.GetVariable("PhilosophyPoint").AsInt;
        MathPoint = DialogueLua.GetVariable("MathPoint").AsInt;

        PhilosophyCredit.text = "Philosophy : " + PhilosophyPoint.ToString();
        MathCredit.text = "Geometry : " + MathPoint.ToString();
    }

    Vector3 GetConversationViewPoint()
    {
        Vector3 midPosition = new Vector3(0, 0, 0);
        midPosition = (trPlayer.position + trConversant.position) / 2;
        float y = midPosition.y - 0.75f;
        midPosition.Set(midPosition.x, y, midPosition.z);
        return midPosition;
    }

    public void ToggleCamMode()
    {
        if(m_CamearaCtrl != null)
        {
            m_CamearaCtrl.ToggleZoomMode();            
        }
            
    }

    public void SetActiveMinimap(bool bActive)
    {
        if (minimapSet == null)
        {
            Debug.Log("ToggleMinimap called but minimapSet is null...");
            return;
        }
        
        minimapSet.SetActive(bActive);
        if (bActive)
        {
            SetNextTargetCounselor();
            ChangeGameMode(GameMode.MINIMAP_MODE);
        }            
        else
        {
            if(PreviousMode != GameMode.MINIMAP_MODE)
                ChangeGameMode(PreviousMode);
            else
                ChangeGameMode(GameMode.PLAY_MODE);
        }   

    }

    public void SetNextTargetCounselor()
    {
        this.trNextTarget = null;
        var gameObjectCounselors = GameObject.FindGameObjectsWithTag("Counselor");
        GameObject targetCounselor = null;
        float shortestDistToCounselor = 0.0f;
        for ( int i = 0 ; i < gameObjectCounselors.Length ; i++ )
        {
            var counselor = gameObjectCounselors[i];
            var isActiveCounselor = false;
            var questIndicator = counselor.GetComponentInChildren<QuestStateIndicator>();
            if (questIndicator != null && questIndicator.indicators != null && questIndicator.indicators.Length > 0)
            {
                var arrIndicators = questIndicator.indicators;                
                //현재는 Quest Indicator 중에서 첫번째 요소는 무조건 비어 있기 때문에 체크하지 않습니다. 두 번째부터가 활성화되어 있는지 체크합니다.
                //두번째부터 지정된 indicator중의 하나가 활성화되어 있다면, 목표로 해야 하는 Target입니다!
                //for (int j = 1; j < arrIndicators.Length; j++)
                //{
                //일단 ? 마크가 있는 걸 목표로 합시다...
                    if (arrIndicators[1].activeInHierarchy) isActiveCounselor = true;                    
                //}                                
            }

            if (isActiveCounselor)
            {
                var distToCounselor = Vector3.Distance(this.trPlayer.position, counselor.transform.position);

                if (shortestDistToCounselor == 0.0f)
                {
                    targetCounselor = counselor;
                    shortestDistToCounselor = distToCounselor;
                }
                else if(shortestDistToCounselor > distToCounselor)
                {
                    targetCounselor = counselor;
                    shortestDistToCounselor = distToCounselor;
                }
            }
        }

        trNextTarget = (targetCounselor != null) ? targetCounselor.transform: null;
    }


}
