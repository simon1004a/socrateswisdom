﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace PrimitiveUI.Examples
{
    public class MathManager : MonoBehaviour
    {
        public static MathManager instance = null;

        public PrimitiveCanvas primitiveCanvas;
        public GameObject MathNote;

        public Color ColorBackground;
        public Color ColorFill;
        public Color ColorStroke;

        StrokeStyle SegmentsStroke;

        //singleton만들자.
        void Awake()
        {
            if(instance == null)
                instance = this;
            else if(instance != this)
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
        }

        // Use this for initialization
        void Start()
        {
            SegmentsStroke = new StrokeStyle(ColorStroke, 2f, StrokeScaleMode.Absolute);
            
        }

        public void DrawLine(Vector2 point1, Vector2 point2)
        {
            primitiveCanvas.DrawLine(point1, point2, SegmentsStroke);
            AddLine(point1, point2);
        }

        public void EnableMath(bool bCanvas)
        {
            //기하학 모드일 때는 뒤 배경에 Rendering은 정지시킨다. performance를 위해서! 
            Time.timeScale = (bCanvas) ? 0 : 1;
            MathNote.SetActive(bCanvas);
            
        }

        //문제 풀이 결과 검토 코드 - 일단 선분의 수직 이등분을 vector로 판단해보자. 
        public enum RESULT_TYPE
        {
            RESULT_POINT = 0,   //결과값으로 점을 요구하는 경우
            RESULT_VECTOR,      //결과값으로 특정 vector, 또는 직선을 요구하는 경우 (선분도 가능하다. )
            RESULT_FIGURE,      //결과값으로 특정 도형을 요구하는 경우. 이건 더 구체적으로 짜야 한다. 
            RESULT_AREA         //결과값으로 특정 넓이를 요구하는 경우. 
        };

        public RESULT_TYPE CurrentResultMode = RESULT_TYPE.RESULT_POINT;

        //결과값이어야 할 vector 담는 변수. 추후 여러 타입을 동시에 관리할 수 있는 구조로 바꾸면 좋을듯 하다.
        Vector4 resultVector = Vector4.zero;
        //맞췄을 경우의 획득 점수
        int resultPoint = 0;
        public void SetResultPoint(int point) { resultPoint = point; }
        public int GetResultPoint() { return resultPoint; }

        public void SetResultVector(Vector4 result)
        {
            CurrentResultMode = RESULT_TYPE.RESULT_VECTOR;
            resultVector = result;
            Debug.Log("Result vector defined : " + resultVector.ToString("F4"));
        }


        public bool IsMathResult()
        {
            bool bResult = false;

            switch (CurrentResultMode)
            {
                case RESULT_TYPE.RESULT_POINT:
                    break;

                case RESULT_TYPE.RESULT_VECTOR:
                    Vector2 PointO = new Vector2(resultVector.x, resultVector.y);
                    float fVecLength = resultVector.z;
                    float fVecDirectAngle = resultVector.w; //degree 단위임을 기억한다. 0부터 360도까지.

                    //Debug.Log("Result Vector should pass " + PointO.ToString() );

                    foreach (var line in Lines)
                    {
                        Vector2 PointA = new Vector2(line.Value.x, line.Value.y);
                        Vector2 PointB = new Vector2(line.Value.z, line.Value.w);

                        //일단 ResultVector의 시작점을 지나는지를 체크한다.
                        if (Mathf.Abs(FunctionOfLine(PointA, PointB, PointO.x, PointO.y)) < MINIMUM_TOLERANCE)
                        {
                            //Debug.Log("PointO "+ PointO.ToString() + "is on the " + line.Key);
                            //작도된 직선이 요구되는 점을 지난다는 사실은 확인했으므로 이제는 기울기가 일치하는지 살펴본다.
                            Vector2 DirVec = PointB - PointA;
                            float fAngle = Mathf.Atan(DirVec.y / DirVec.x) * Mathf.Rad2Deg;

                            //직선의 방향각이 일치하거나 또는 180도 차이이면 기울기가 같은 것이다!
                            if (Mathf.Abs(fAngle - fVecDirectAngle) < MINIMUM_TOLERANCE
                              || Mathf.Abs(Mathf.Abs(fAngle - fVecDirectAngle) - 180.0f) < MINIMUM_TOLERANCE)
                            {
                                if (fVecLength == 0)
                                {
                                    //vector길이가 0입력된 경우는 벡터의 크기를 고려하지 않는다. 
                                    Debug.Log("결과 vector와 일치하는 직선이 발견되었습니다.(크기 제외)");
                                    bResult = true;
                                }
                                else
                                {
                                    if (Mathf.Abs(DirVec.magnitude - fVecLength) < MINIMUM_TOLERANCE)
                                    {
                                        Debug.Log("결과 vector와 크기까지 완벽하게 일치하는 선분이 발견되었습니다.");
                                        bResult = true;
                                    }
                                }
                            }
                        }
                    }

                    break;

                case RESULT_TYPE.RESULT_FIGURE:
                    break;

                case RESULT_TYPE.RESULT_AREA:
                    break;

            }



            return bResult;
        }




        /*************작도 기하들 데이터 저장하고 그 사이 좌표 계산하는 코드들!!!
        
            1.순수한 수학적 계산만 한다.
            2.계산은 물론 Canvas위에서 일어나는 것이지만, 캔버스에 붙인 스크립트가 아니라
            이 MathManager singleton에서 한다 -> 그래야 나중에 결과값을 검토하기가 쉬워진다.
            결과값 검토를 MathManager에서 검사하기 위해서 이렇게 하는 것이다!
            3. 결과로 나온 제일 중요한 교차점들은 Primitive Canvas에서 가져가서 
            교차점들을 표시할 수 있게 한다.
            4. 작도는 낙장 불입으로 하자-> 하나씩 undo하는 건 나중에!

            ************************************************************/

        //float숫자 끼리 비교할 때 그 차이가 이 수치보다 더 작으면 두 숫자는 같다고 간주한다.
        const float MINIMUM_TOLERANCE = 0.0001f;   

        //일단 한 번 그린 도형은 지울 수 없게 한다. 만약 지우고 싶다면 처음부터 다시 한다!
        //주요 기하 도형들을 Dictionary 형태로 저장한다. 이 데이터는 유저가 그리는 데이터에 한하도록 한다. 
        Dictionary<string, Vector3> Circles = new Dictionary<string, Vector3>();
        int CircleIndex = 0;    //Circle이 생성될 때마다 1씩 증가한다. key값으로 사용한다 - circle이 일부 삭제되어도 이 값은 불변이다!

        Dictionary<string, Vector4> Lines = new Dictionary<string, Vector4>();
        int LineIndex = 0;      //Line이 생성될 때마다 1씩 증가한다. key값으로 사용한다 -  line이 일부 삭제되어도 이 값은 불변이다!

        Dictionary<string, Vector2> Points = new Dictionary<string, Vector2>();
        int PointIndex = 0;     //Point가 생성될 때마다 1씩 증가한다.key값으로 사용한다 -  일부 point가 삭제되어도 이 값은 불변이다! 


        public Dictionary<string, Vector2> GetPoints() { return Points; }


        //계산할 때 쓰는 좌표계는 항상 상대 좌표계이다.(Relative Coordinate)
        public void AddCircle(Vector2 point, float rad)
        {
            //float를 3자리까지 truncate하자!
            //point.x = Mathf.Round(point.x * 1000f) / 1000f;
            //point.y = Mathf.Round(point.y * 1000f) / 1000f;
            //rad = Mathf.Round(rad * 1000f) / 1000f;

            Vector3 newCircle = new Vector3(point.x, point.y, rad);
            string NewCircleKey = "Circle" + CircleIndex.ToString();
            CircleIndex++;

            //Debug.Log( NewCircleKey + " Added! :" + newCircle.ToString("F4"));

            //데이터 상으로 원의 정보를 일단 저장해둔다!
            Circles.Add(NewCircleKey, newCircle);
            //원의 중심점도 중요한 점들이니 더하도록 한다. 
            AddPoint(point);

            CalculateNewPointsCircleWithLines(NewCircleKey);
            CalculateNewPointsCircleWithCircles(NewCircleKey);
        }

        public void AddLine(Vector2 pointA, Vector2 pointB)
        {
            //pointA.x = Mathf.Round(pointA.x * 1000f) / 1000f;
            //pointA.y = Mathf.Round(pointA.y * 1000f) / 1000f;
            //pointB.x = Mathf.Round(pointB.x * 1000f) / 1000f;
            //pointB.y = Mathf.Round(pointB.y * 1000f) / 1000f;

            Vector4 newLine = new Vector4(pointA.x, pointA.y, pointB.x, pointB.y);
            string NewLineKey = "Line" + LineIndex.ToString();
            LineIndex++;

            Lines.Add(NewLineKey, newLine);

            //Debug.Log(NewLineKey + " Added! :" + newLine.ToString("F4"));
            //선의 두 끝점도 중요한 점들이긴 하니 더하긴 하자.
            AddPoint(pointA);
            AddPoint(pointB);

            //새로 생긴 선분에 대해서, 새로 생긴 교차점들을 구한다. 
            CalculateNewPointsLineWithLines(NewLineKey);
            CalculateNewPointsLineWithCircles(NewLineKey);            
        }

        private void AddPoint(Vector2 point)
        {
            //기존 점들과 비교해봐서 거의 일치하는 것이 있으면 굳이 더하지 않는다. 
            foreach (var pt in Points)
            {
                float dist = Vector2.Distance(pt.Value, point);
                if (dist < MINIMUM_TOLERANCE)
                    return;
            }

            //point.x = Mathf.Round(point.x * 1000f) / 1000f;
            //point.y = Mathf.Round(point.y * 1000f) / 1000f;

            string NewPointKey = "Point" + PointIndex.ToString();            
            Points.Add(NewPointKey, point);
            PointIndex++;

            Debug.Log( NewPointKey + " added!! " + point.ToString("F4") );
            //테스트 겸 삼아 한 번 작게 점을 찍어 보자.
            primitiveCanvas.DrawCircle(point, 0.0035f, Color.black);

        }


        public void ClearFigures()
        {
            primitiveCanvas.Clear();

            Circles.Clear();
            Lines.Clear();
            Points.Clear();

            CircleIndex = 0;
            LineIndex = 0;
            PointIndex = 0;

            resultVector = Vector4.zero;
        }

        //도형 사이의 교차점들 구하는 함수들.


        //원 하나와 원들 사이의 점들을 새로 계산한다.
        public void CalculateNewPointsCircleWithCircles(string NewCircleName)
        {
            Vector3 circle = Circles[NewCircleName];

            foreach (var cir in Circles)
            {
                Vector2 NewPt1, NewPt2;
                if (cir.Key == NewCircleName)
                    continue;

                switch (CalculateNewPointsCircleAndCircle(circle, cir.Value, out NewPt1, out NewPt2))
                {
                    case 0:     //교차점이 없는 경우
                        break;
                    case 1:     //교차점이 하나인 경우
                        AddPoint(NewPt1);
                        break;
                    case 2:     //교차점이 둘인 경우
                        AddPoint(NewPt1);
                        AddPoint(NewPt2);
                        break;
                }
            }
        }


        public int CalculateNewPointsCircleAndCircle(Vector3 circle1, Vector3 circle2, out Vector2 newpoint1, out Vector2 newpoint2)
        {
            Vector2 center1 = new Vector2(circle1.x, circle1.y);
            float r1 = circle1.z;

            Vector2 center2 = new Vector2(circle2.x, circle2.y);
            float r2 = circle2.z;

            //원의 중심 사이를 연결하는 벡터를 우선 기준으로 삼아서 , 피타고라스 정리로 우선 현과 중심선 사이의 교점부터 구한다. 자세한 건 노트!!!
            Vector2 DirVec = center2 - center1;

            //만약 원의 중심이 서로 완벽히 일치하면 그냥 교점이 없는 것으로 한다. 서로 완벽히 일치하면 무한히 많은 점이 있을 것이나 딱히 처리할 필요는 없다.
            if (Vector2.Distance(DirVec, Vector2.zero) < MINIMUM_TOLERANCE)
            {
                Debug.Log("두 원의 중심이 일치합니다.");
                newpoint1 = Vector2.zero;
                newpoint2 = Vector2.zero;
                return 0;
            }

            float dist = Vector2.Distance(center1, center2);

            //Debug.Log("Dist is " + dist + " and r1 and r2 is " + r1 + " " + r2 );

            if (Mathf.Abs(dist - (r1 + r2)) < MINIMUM_TOLERANCE)      //원과 원이 서로 접한다.
            {
                Debug.Log("두 원이 서로 정확하게 접합니다!!!");
                float s = r1 / dist;
                newpoint1 = center1 + s * DirVec;
                newpoint2 = Vector2.zero;
                return 1;
            }

            if (dist > (r1 + r2))     //원과 원이 서로 만나지 않는다.
            {
                newpoint1 = Vector2.zero;
                newpoint2 = Vector2.zero;
                return 0;
            }
            else if (dist < Mathf.Abs(r1 - r2))   //한 원이 다른 원에 포함되는 경우
            {
                newpoint1 = Vector2.zero;
                newpoint2 = Vector2.zero;
                return 0;
            }
            else      //원과 원이 만나 두 개의 교차점을 가진다.
            {
                float t = (1.0f - ((r2 * r2 - r1 * r1) / Vector2.SqrMagnitude(DirVec))) / 2.0f;
                Vector2 CrossVec = center1 + t * DirVec;
                float realLengthOft = (t * DirVec).magnitude;
                float k = Mathf.Sqrt(r1 * r1 - realLengthOft * realLengthOft);
                Vector2 n = new Vector2(-DirVec.y, DirVec.x);
                n = n.normalized;

                newpoint1 = CrossVec + k * n;
                newpoint2 = CrossVec - k * n;
                return 2;
            }
        }

        //원 하나와 선들 사이의 점들을 새로 계산한다.
        public void CalculateNewPointsCircleWithLines(string NewCircleName)
        {
            //비교할 원
            Vector3 circle = Circles[NewCircleName];
            
            foreach (var line in Lines)
            {
                Vector2 NewPt1, NewPt2;
                switch (CalculateNewPointsLineAndCircle(line.Value, circle, out NewPt1, out NewPt2))
                {
                    case 0:     //교차점이 없는 경우
                        break;
                    case 1:     //교차점이 하나인 경우
                        AddPoint(NewPt1);
                        break;
                    case 2:     //교차점이 둘인 경우
                        AddPoint(NewPt1);
                        AddPoint(NewPt2);
                        break;
                }
            }
        }


        //선 하나와 원들 사이의 점들을 계산한다. 
        public void CalculateNewPointsLineWithCircles(string NewLineName)
        {
            //비교할 선
            Vector4 line = Lines[NewLineName];
            
            foreach (var circle in Circles)
            {
                Vector2 NewPt1, NewPt2;
                switch (CalculateNewPointsLineAndCircle(line, circle.Value, out NewPt1, out NewPt2))
                {
                    case 0:     //교차점이 없는 경우
                        break;
                    case 1:     //교차점이 하나인 경우
                        AddPoint(NewPt1);                        
                        break;
                    case 2:     //교차점이 둘인 경우
                        AddPoint(NewPt1);
                        AddPoint(NewPt2);
                        break;
                }
            }
        }

        //교차점의 개수가 두 개면 2를, 하나면 1을, 없으면 0을 return한다.
        public int CalculateNewPointsLineAndCircle(Vector4 line, Vector3 circle, out Vector2 newpoint1, out Vector2 newpoint2)
        {
            //선분의 시작점과 끝점의 좌표 확보
            Vector2 BeginPt = new Vector2(line.x, line.y);
            Vector2 EndPt = new Vector2(line.z, line.w);

            //원의 중심과 반지름 확보
            Vector2 CenterPt = new Vector2(circle.x, circle.y);
            float r = circle.z;

            //직선과 원점에서 직선에 내린 수선의 발과의 교차점 변수
            Vector2 CrossPoint = Vector2.zero;

            float dist = 0.0f;  //점과 직선 사이의 최단 거리 나타내는 변수.
            float dist1 = Vector2.Distance(CenterPt, BeginPt);
            float dist2 = Vector2.Distance(CenterPt, EndPt);

            if (Mathf.Abs(dist1 - r) < MINIMUM_TOLERANCE || Mathf.Abs(dist2 - r) < MINIMUM_TOLERANCE)        //두 점 중 하나가 원 위에 위치하는 경우
            {
                //Debug.Log("점이 정확하게 원 위에 위치합니다!!!!" + "원 중심은 " + CenterPt.ToString());
                //두 점이 모두 원 위에 위치하는 경우
                if ((Mathf.Abs(dist1 - r) < MINIMUM_TOLERANCE) && (Mathf.Abs(dist2 - r) < MINIMUM_TOLERANCE))
                {
                    newpoint1 = BeginPt;
                    newpoint2 = EndPt;
                    Debug.Log("두 점이 모두 정확히 원 위에 위치합니다!");

                    return 2;
                }
                else    //양 끝점 중 하나만 원 위에 위치하는 경우. - 좀 너무 하드코딩한 듯한 느낌...썩 효율적으로 짠 것 같지는 않다만...ㅠㅜ 우선 둔다. 16.7.5. 화.
                {
                    //시작점 하나만 원 위에 위치하고 끝점은 원 안에 있는 경우.
                    if ((Mathf.Abs(dist1 - r) < MINIMUM_TOLERANCE) && (dist2 < r))
                    {
                        newpoint1 = BeginPt;
                        newpoint2 = Vector2.zero;
                        Debug.Log("시작점 하나가 정확히 원 위에 위치하고 나머지 하나는 원 안에 있습니다!");
                        return 1;
                    }

                    //끝점 하나만 원 위에 있고 시작점은 원 안에 있는 경우.
                    if ((Mathf.Abs(dist2 - r) < MINIMUM_TOLERANCE) && (dist1 < r))
                    {
                        //끝점 하나만 원 위에 위치하는 경우.
                        newpoint1 = EndPt;
                        newpoint2 = Vector2.zero;
                        Debug.Log("끝점 하나가 정확히 원 위에 위치하고 나머지 하나는 원 안에 있습니다!");

                        return 1;
                    }

                    //한 점은 원주 위에 있으나 나머지 한 점이 원 밖에 있는 경우. 
                    //일단 원과 직선의 교차점 구하는 식을 써본다.
                    dist = DistanceBetweenLineAndPt(line, CenterPt, out CrossPoint);

                    float AmpVec = Mathf.Sqrt(r * r - dist * dist);
                    Vector2 DirVec;
                    if (Mathf.Abs(dist1 - r) < MINIMUM_TOLERANCE)
                    {
                        newpoint1 = BeginPt;
                        DirVec = (EndPt - BeginPt).normalized;  //시작점이 원 위에 위치하는 경우의 방향 벡터
                    }
                    else
                    {
                        newpoint1 = EndPt;
                        DirVec = (BeginPt - EndPt).normalized;  //끝점이 원 위에 위치하는 경우의 방향 벡터
                    }

                    Vector2 NewPt = CrossPoint + AmpVec * DirVec;

                    //새로 구한 교차점이 그냥 원래 원주 위의 점과 같다면 그냥 교차점이 한 개인 것이니 그냥 1로 return한다.
                    if (Vector2.Distance(newpoint1, NewPt) < MINIMUM_TOLERANCE)
                    {
                        newpoint2 = Vector2.zero;
                        Debug.Log("직선의 양 끝점 중 하나가 정확히 원 위에 위치합니다!");
                        return 1;
                    }
                    else
                    {
                        newpoint2 = NewPt;
                        Debug.Log("두 교차점 중 하나는 정확히 원주 위에 위치합니다!");
                        return 2;
                    }
                }
            }

            //두 점 중에 하나만 원 안에 포함되는 경우
            if ((dist1 - r) * (dist2 - r) < 0)
            {
                dist = DistanceBetweenLineAndPt(line, CenterPt, out CrossPoint);

                //교차점이 반드시 단 하나 존재하므로, '점과 직선 사이 거리'와 반지름을 비교할 필요는 없다.
                float AmpVec = Mathf.Sqrt(r * r - dist * dist);
                Vector2 DirVec = (EndPt - BeginPt).normalized;
                Vector2 NewPt = CrossPoint + AmpVec * DirVec;

                if (dist1 > r)  //시작점이 원 밖에 있는 경우
                {
                    //DirVec방향이 Endpt를 향하므로, 시작점이 원 밖이면 방향을 바꾸어야 한다.
                    NewPt = CrossPoint - AmpVec * DirVec;
                }

                //Debug.Log("Cross Point is only 1" + NewPt.ToString());
                newpoint1 = NewPt;
                newpoint2 = Vector2.zero;
                return 1;
            }
            else if ((dist1 > r) && (dist2 > r))  //두 점이 모두 원 밖에 위치하는 경우
            {
                //Debug.Log("점 둘이 모두 원 밖에 있습니다.");
                //원의 중심에서 직선에 내린 수선의 길이가 반지름보다 짧다면 교차점이 있다.
                dist = DistanceBetweenLineAndPt(line, CenterPt, out CrossPoint);
                //Debug.Log("원의 중심과 선 사이의 거리는 " + dist + "이고 반지름 r은 " + r);

                if (Mathf.Abs(r - dist) < MINIMUM_TOLERANCE)      //접하는 경우!
                {
                    Debug.Log("직선과 원이 접하는 경우입니다!!!!!!!!");
                    newpoint1 = CrossPoint;
                    newpoint2 = Vector2.zero;
                    return 1;
                }
                else if (r > dist)
                {
                    float AmpVec = Mathf.Sqrt(r * r - dist * dist);
                    Vector2 DirVec = (EndPt - BeginPt).normalized;

                    //직선이 아닌 선분이므로, 한 번 더 체크. 선분 전체가 원 밖에 있는 건 아닌지 체크해본다.
                    if (FunctionOfLine(CenterPt, CrossPoint, BeginPt.x, BeginPt.y) * FunctionOfLine(CenterPt, CrossPoint, EndPt.x, EndPt.y) < 0)
                    {
                        //수선의 발의 방정식이 선분을 나누고 있는 것이 확실하므로 이제 선과 원의 교차점이 반드시 두 개 존재한다.
                        newpoint1 = CrossPoint + AmpVec * DirVec;
                        newpoint2 = CrossPoint - AmpVec * DirVec;
                        //Debug.Log("Cross Point are 2- " + newpoint1.ToString() + " and " + newpoint2.ToString());

                        return 2;
                    }
                }
            }

            newpoint1 = Vector2.zero;
            newpoint2 = Vector2.zero;
            return 0;
        }


        //점과 직선 사이의 거리를 구하는 함수. 점에서 직선에 수선의 발을 내렸을 때 교차점의 좌표도 동시에 구한다. 
        public float DistanceBetweenLineAndPt(Vector4 line, Vector2 point, out Vector2 crosspt)
        {
            float dist = 0.0f;

            //직선의 방향 벡터와 수선이 서로 orthogonal하다는 걸 이용해서 선형대수로 푼다. 자세한 건 노트에.
            Vector2 RightVec = point - new Vector2(line.x, line.y);
            Vector2 DirVec = new Vector2(line.z, line.w) - new Vector2(line.x, line.y);
            DirVec = DirVec.normalized;                             //단위 벡터로 만든다.
            Vector2 NorVec = new Vector2(-DirVec.y, DirVec.x);      //90도 회전시켜서 수선의 방향 벡터 만든다.

            Vector4 Mtx2By2 = new Vector4(DirVec.x, NorVec.x, DirVec.y, NorVec.y);
            Vector4 InverMtx = (new Vector4(Mtx2By2.w, -Mtx2By2.y, -Mtx2By2.z, Mtx2By2.x)) * (1 / (Mtx2By2.x * Mtx2By2.w - Mtx2By2.y * Mtx2By2.z));

            float t = (InverMtx.x * RightVec.x) + (InverMtx.y * RightVec.y);
            float k = (InverMtx.z * RightVec.x) + (InverMtx.w * RightVec.y);

            dist = Mathf.Abs(k);

            crosspt = (new Vector2(line.x, line.y)) + t * DirVec;
            //Debug.Log("Cross Point is " + crosspt.ToString()); 

            return dist;
        }
        


        //선과 선 사이의 점들을 새로 계산한다.
        public void CalculateNewPointsLineWithLines(string newLineName)
        {
            //새로 추가된 선과 나머지 선들 사이에 교차점이 있는지 검토한다. 
            foreach (var line in Lines)
            {
                //자기 자신을 제외한 다른 선들과의 교차점들을 계산한다. 
                if (newLineName != line.Key)
                {
                    if ( IsLineCrossed(Lines[newLineName], line.Value) )
                    {
                        //교차되는 것이 확실하니 교차점을 구한다. 선과 선 사이이므로 교차점은 하나다.
                        Vector2 NewPt = CrossPtBetweenLine(Lines[newLineName], line.Value);
                        AddPoint(NewPt);
                    }
                }
            }
        }


        public Vector2 CrossPtBetweenLine(Vector4 LineA, Vector4 LineB)
        {
            Vector2 CrossPt = new Vector2();

            Vector2 BeginPtOfA = new Vector2(LineA.x, LineA.y);
            Vector2 EndPtOfA = new Vector2(LineA.z, LineA.w);

            Vector2 BeginPtOfB = new Vector2(LineB.x, LineB.y);
            Vector2 EndPtOfB = new Vector2(LineB.z, LineB.w);

            //벡터를 이용해서, 두 선분이 서로를 내분함을 이용해서 계산한다. 자세한 계산 과정은 노트에...
            //2By2 Mtx를 만들기 위해서 변수 4개를 우선 계산한다...
            Vector4 Mtx2By2 = new Vector4((EndPtOfA - BeginPtOfA).x, -(EndPtOfB - BeginPtOfB).x,
                (EndPtOfA - BeginPtOfA).y, -(EndPtOfB - BeginPtOfB).y);
            Vector2 RightVec = BeginPtOfB - BeginPtOfA;

            Vector4 InverMtx = (new Vector4(Mtx2By2.w, -Mtx2By2.y, -Mtx2By2.z, Mtx2By2.x)) * (1 / (Mtx2By2.x * Mtx2By2.w - Mtx2By2.y * Mtx2By2.z));

            float t = (InverMtx.x * RightVec.x) + (InverMtx.y * RightVec.y);
            //float s = (InverMtx.z * RightVec.x) + (InverMtx.w * RightVec.y);

            CrossPt = BeginPtOfA + t * (EndPtOfA - BeginPtOfA);
            //Debug.Log("교차점은 " + CrossPt.ToString() + "다르게 계산한 교차점도 " + (BeginPtOfB + s * (EndPtOfB - BeginPtOfB)).ToString());

            return CrossPt;
        }

        public bool IsLineCrossed(Vector4 lineA, Vector4 lineB)
        {
            Vector2 BeginPtOfA = new Vector2(lineA.x, lineA.y);
            Vector2 EndPtOfA = new Vector2(lineA.z, lineA.w);

            Vector2 BeginPtOfB = new Vector2(lineB.x, lineB.y);
            Vector2 EndPtOfB = new Vector2(lineB.z, lineB.w);
            
            //일단 두 선이 평행하거나, 일치하는 경우에는 교차점이 없는 것으로 한다.
            if (Vector2.Distance((EndPtOfA - BeginPtOfA).normalized, (EndPtOfB - BeginPtOfB).normalized) < MINIMUM_TOLERANCE)
            {
                Debug.Log("두 직선이 평행합니다.");
                return false;
            }

            //일단 선A의 직선의 방정식에 대해 B의 시작점과 끝점이 나뉘어지는지를 살펴보고
            if (FunctionOfLine(BeginPtOfA, EndPtOfA, BeginPtOfB.x, BeginPtOfB.y) * FunctionOfLine(BeginPtOfA, EndPtOfA, EndPtOfB.x, EndPtOfB.y) < 0)
            {
                //또다시 같은 식으로 선B의 직선의 방정식에 대해서도 A의 시작점과 끝점이 나뉘어지는지 살펴본다.
                if (FunctionOfLine(BeginPtOfB, EndPtOfB, BeginPtOfA.x, BeginPtOfA.y) * FunctionOfLine(BeginPtOfB, EndPtOfB, EndPtOfA.x, EndPtOfA.y) < 0)
                {
                    //Debug.Log("Line is crossing!!!" );
                    return true;
                }
            }

            return false;
        }


        //이 함수의 return값이 양이면 점(x,y)가 직선 AB 위쪽 평면에, 음이면 점이 직선 아래 평면에 있다 할 수 있다. 0이면 직선 위에 있다!
        float FunctionOfLine(Vector2 A, Vector2 B, float x, float y)
        {
            float ValueOfPointInFunc = 0.0f;

            //점 A와 B를 지나는 일차 함수를 구해서 음함수 모양으로 만들어, x,y가 위쪽에, 아래쪽에 있는지 판단한다.
            //기울기가 무한대일 경우 (수직)는 따로 예외처리로 해주자.
            if (Mathf.Abs(A.x - B.x) < MINIMUM_TOLERANCE)
            {
                //점이 오른쪽에 있으면 양, 왼쪽이면 음이 되겠다.
                ValueOfPointInFunc = x - A.x;
            }
            else
            {
                float gradient = (B.y - A.y) / (B.x - A.x);
                ValueOfPointInFunc = gradient * (x - A.x) - y + A.y;
            }           

            return ValueOfPointInFunc;
        }











    }

}
