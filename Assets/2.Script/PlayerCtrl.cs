﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerCtrl : MonoBehaviour {

    public enum PlayerMode
    {
        NAVI_MOVE_MODE,
        STICK_MOVE_MODE,
    }

    public VirtualJoystick moveJoystick;
    protected Animator animator;

    public float MoveSpeed = 3.7f;
    public float RotationSpeed = 100.0f;

    float translation = 0.0f;
    float rotation = 0.0f;


    //Navigation 기능 추가 - 조망 모드에서 클릭한 Ground로 이동하도록 조치함. 
    private NavMeshAgent nvAgent;
    private CameraCtrl camCtrl;
    PlayerMode CurrentCtrlMode;

    void Start()
    {
        animator = GetComponent<Animator>();
        moveJoystick = GameObject.FindGameObjectWithTag("GameController").GetComponent<VirtualJoystick>();

        nvAgent = GetComponent<NavMeshAgent>();
        nvAgent.enabled = false;
        camCtrl = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraCtrl>();
        CurrentCtrlMode = PlayerMode.STICK_MOVE_MODE;

    }

    // Update is called once per frame
    void Update ()
    {
        //가끔씩 떨어지는 버그가 있어서 수정차...
        if (transform.position.y < -1.0f)
        {
            Debug.Log("Player is falling!");
            transform.position.Set(transform.position.x, 1.0f, transform.position.z);
        }

        if ( GameManager.instance.CurrentMode == GameManager.GameMode.PLAY_MODE
          || GameManager.instance.CurrentMode == GameManager.GameMode.MINIMAP_MODE )
        {
            translation = Input.GetAxis("Vertical");
            rotation = Input.GetAxis("Horizontal");

            //모바일 컨트롤러로부터 Input받기.
            if (moveJoystick.InputDirection != Vector3.zero)
            {
                translation = moveJoystick.InputDirection.z;
                rotation = moveJoystick.InputDirection.x;
            }
        }
        else
        {
            translation = 0f;
            rotation = 0f;
        }


        if (camCtrl.IsZoomIn == false)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Input.GetButtonDown("Fire2"))
            {
                Debug.Log("Fire2 button Clicked!!!");
                if (Physics.Raycast(ray, out hit, 1000))
                {
                    //레이캐스트가 캐릭터 근처이면 Conversant캐릭터이므로 이동되지 않음을 유의한다.
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Ground"))
                    {
                        CurrentCtrlMode = PlayerMode.NAVI_MOVE_MODE;
                        nvAgent.enabled = true;
                        nvAgent.SetDestination(hit.point);
                        Debug.Log("Current Player Moving Mode is " + CurrentCtrlMode.ToString());
                    }
                }
            }
        }
       
        //네비게이션 모드 중이라도 input이 들어오면 그냥 바로 수동 컨트롤 모드로 바꾸자. 
        if (translation != 0 || rotation != 0)
        {
            CurrentCtrlMode = PlayerMode.STICK_MOVE_MODE;            
            nvAgent.enabled = false;
        }
        


        if (CurrentCtrlMode == PlayerMode.STICK_MOVE_MODE)
        {
            //Speed가 있으면 걷는 동작으로 바뀔 것이다.
            animator.SetFloat("Speed", Mathf.Abs(translation * MoveSpeed));

            Vector3 moveDir = Vector3.forward * translation;
            transform.Translate(moveDir * Time.deltaTime * MoveSpeed, Space.Self);
            transform.Rotate(Vector3.up * Time.deltaTime * RotationSpeed * rotation);
        }
        else if(CurrentCtrlMode == PlayerMode.NAVI_MOVE_MODE)
        {            
            animator.SetFloat("Speed", nvAgent.velocity.magnitude);
            if(Vector3.Distance(transform.position, nvAgent.destination) < 0.1f)
            {                
                CurrentCtrlMode = PlayerMode.STICK_MOVE_MODE;
                Debug.Log("Current Player Moving Mode is " + CurrentCtrlMode.ToString());
            }
        }
		
	}

    public void OnConversationStart()
    {
        GameManager.instance.StartConversation();
    }

    public void OnConversationEnd()
    {
        GameManager.instance.EndConversation();

    }
}
