﻿using PrimitiveUI.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PixelCrushers.DialogueSystem
{
    public class CheckGeometry : MonoBehaviour
    {
        /// <summary>
        /// The variable to update.
        /// </summary>
        [Tooltip("Update this Dialogue System variable when the input signal entered.")]
        public string variable = string.Empty;


        [Tooltip("Optional alert message to show when updating.")]
        public string alertMessage = string.Empty;


        protected string ActualVariableName
        {
            get { return string.IsNullOrEmpty(variable) ? OverrideActorName.GetInternalName(transform) : variable; }
        }

        public void OnCheckResult()
        {
            bool bResult = MathManager.instance.IsMathResult();
            Debug.Log("Geometry Construction Result is " + bResult.ToString());

            //Player와 Actor를 찾아서 문제 풀이 결과에 따라 모션을 달리해보자.
            Transform trPlayer = GameManager.instance.GetPlayerTransform();
            Transform trConversant = GameManager.instance.GetConversantTransform();

            Debug.Log("Current Actor is " + trConversant);

            if (bResult)
            {
                //DialogueLua.SetVariable("MathPoint").AsInt;
                int mathpoint = DialogueLua.GetVariable("MathPoint").AsInt;
                mathpoint += MathManager.instance.GetResultPoint();
                DialogueLua.SetVariable("MathPoint", mathpoint);
                GameManager.instance.UpdatePoint();

                GameManager.instance.PlaySuccess();
                trPlayer.GetComponent<Animator>().SetTrigger("Good");
                if (trConversant != null)
                    trConversant.GetComponent<Animator>().SetTrigger("Positive");
            }
            else
            {
                GameManager.instance.PlayLoss();               
                trPlayer.GetComponent<Animator>().SetTrigger("Bad");
                if (trConversant != null)
                    trConversant.GetComponent<Animator>().SetTrigger("Negative");
            }

            MathManager.instance.SetResultPoint(0);

            //int oldValue = DialogueLua.GetVariable(ActualVariableName).AsInt;
            //int newValue = 1;
            //string Quest = GameMgr.GetQuest();
            //Debug.Log("CheckGeometry checks the quest " + Quest);
            //if (bResult)
            //    DialogueLua.SetQuestField(Quest, "State", "success");
            //else
            //    DialogueLua.SetQuestField(Quest, "State", "fail");
            ////DialogueLua.SetVariable(ActualVariableName, newValue);


            //DialogueManager.SendUpdateTracker();
            //if (!(string.IsNullOrEmpty(alertMessage) || DialogueManager.Instance == null))
            //{
            //    DialogueManager.ShowAlert(alertMessage);
            //}

            //결과값을 다 체크했으니 이제 다시 Canvas를 disable시키자.( SetActive(false) ) 

            GameManager.instance.StartCoroutine(
                GameManager.instance.ChangeGameModeLater(GameManager.GameMode.PLAY_MODE, 3f));
            MathManager.instance.EnableMath(false);

        }
       
    }

}

