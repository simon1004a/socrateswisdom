﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleModify : MonoBehaviour {

    public RectTransform CircleRect;

	// Use this for initialization
	void Awake()
    {
        CircleRect = GetComponent<RectTransform>();		
	}

    //인자는 캔버스의 절대 좌표계
    public void ResizeCircleFigure(float rad)
    {
        float diameter = rad * 2.0f;        
        CircleRect.sizeDelta = new Vector2(diameter, diameter);
    }
}
