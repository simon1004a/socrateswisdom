﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineModify : MonoBehaviour {

    public RectTransform LineRect;

    private Vector2 RotateDirection;
    private float DegreeAngle = 0.0f;

    // Use this for initialization
    void Awake()
    {
        LineRect = GetComponent<RectTransform>();
    }

    //새로 입력된 끝점 좌표에 맞춰서 선분의 길이 조절하고 회전한다.인자는 절대 좌표계.
    public void ResizeLineFigure(Vector2 NewEndPt)
    {
        //길이를 조절한 다음에
        float LineLength = Vector2.Distance(NewEndPt, transform.position);
        LineRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, LineLength);

        //그리고 회전한다.
        RotateDirection = NewEndPt - (Vector2)transform.position;
        DegreeAngle = Mathf.Atan2(RotateDirection.y, RotateDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(DegreeAngle, Vector3.forward);
    }

}
