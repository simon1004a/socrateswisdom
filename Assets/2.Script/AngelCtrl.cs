﻿using PixelCrushers.DialogueSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelCtrl : MonoBehaviour {

    public enum AngelMode
    {

    }

    public Transform target;   //날아갈 목표 지점
    public float movementSpeed = 5f;
    public float rotationalDamp = 1f;
    
    
    bool isArrived = false;     //player근처에 다 날아왔는지 체크
    bool isGrounded = false;      //지면에 착지했는지 알아보는 코드


    protected Rigidbody angelRigid;
    protected Animator animator;

    public AudioClip meetAngel1;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        angelRigid = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();                
    }

    // Update is called once per frame
    void Update ()
    {
        isGrounded = (transform.position.y < 1f) ? true : false;
        if ( !isGrounded && !isArrived )
        {
            //Debug.Log("Angel is moving!!!");
            Turn();
            Move();
        }		
	}
    

    private void OnTriggerEnter(Collider other)
    {   
        if (other.tag == "Player" && !isArrived )
        {
            isArrived = true;

            GameManager.instance.PlaySingle(meetAngel1);
            //착지하는 코드 - 자세 바로 잡고 그 다음에 물리엔진에 의해 아래로 내려오자.
            angelRigid.useGravity = true;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            angelRigid.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player" && isGrounded)
        {
            transform.LookAt(target);
        }
    }

    //땅과 충돌할 때 불린다.
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            isGrounded = true;
            animator.SetBool("isGround", isGrounded);
            DialogueManager.StartConversation("Angel");
        }
    }

    void Turn()
    {
        //목표 지점을 바라보고 그쪽을 향하는 코드.
        Vector3 pos = target.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(pos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationalDamp * Time.deltaTime);
    }

    void Move()
    {
        //목표 지점을 향해서 날아가는 코드.
        transform.position += transform.forward * movementSpeed * Time.deltaTime;
    }    


}
