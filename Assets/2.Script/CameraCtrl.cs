﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrl : MonoBehaviour {


    private Transform target;
    private Vector3 targetPos, targetDirection;

    public float FarDistance = 8.0f;
    public float FarHeight = 5.0f;
    public float CloseDistance = 3.0f;
    public float CloseHeight = 2.0f;
    public bool IsZoomIn = false;

    public float dampTrace = 20.0f;     //부드러운 추적을 위한 변수


    float dist = 0.0f;
    float height = 0.0f;

    private void Start()
    {
        IsZoomIn = GameManager.instance.bCamZoomMode;
    }

    public void SetCameraTarget(Transform camTarget)
    {
        target = camTarget;
    }

    public void SetCameraTarget(Vector3 position, Vector3 direction)
    {
        targetPos = position;
        targetDirection = direction;
        //Debug.Log("Camera set changed for two people!");
    }

    public void ToggleZoomMode()
    {
        IsZoomIn = !IsZoomIn;
        GameManager.instance.bCamZoomMode = IsZoomIn;
    }

    // Update is called once per frame
    void LateUpdate ()
    {
        Vector3 ChangeCam;
        Vector3 Target3D;        
        Vector3 CamDirection;

        if (GameManager.instance.CurrentMode == GameManager.GameMode.PLAY_MODE)
        {
            Target3D = target.position;
            CamDirection = target.forward;
        }
        else
        {
            //Debug.Log("Camera Mode is " + GameManager.instance.CurrentMode);
            Target3D = targetPos;
            CamDirection = targetDirection;
        }


        if (Input.GetButtonDown("Jump"))
            ToggleZoomMode();


        if (IsZoomIn)
        {
            dist = CloseDistance;
            height = CloseHeight;
        }
        else
        {
            dist = FarDistance;
            height = FarHeight;
        }

        //카메라 
        ChangeCam = Target3D - (CamDirection * dist) + (Vector3.up * height);
        //Linear interpolation
        transform.position = Vector3.Lerp(transform.position, ChangeCam, Time.deltaTime * dampTrace);

        //장애물에 가릴 경우 피하는 코드 - 이 때 장애물은 Layer가 Ground 또는 Wall로 설정되어 있어야 한다.
        if (IsZoomIn)
        {
            RaycastHit hitInfo;
            if (Physics.Linecast(Target3D + (Vector3.up * 2.0f), transform.position, out hitInfo,
                (1 << LayerMask.NameToLayer("Ground") | 1 << LayerMask.NameToLayer("Wall"))))
            {
                transform.position = hitInfo.point;
            }
        }

        transform.LookAt(Target3D + (Vector3.up * 1.7f));
    }


}
