﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleMapButton : MonoBehaviour {

    public void SetActiveMinimap(bool bActive)
    {
        GameManager.instance.SetActiveMinimap(bActive);
    }
	
}
