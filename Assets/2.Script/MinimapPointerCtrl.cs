﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using UnityEngine.UI;

public class MinimapPointerCtrl : MonoBehaviour
{
    public Transform trPlayer;
    private Transform trTarget;
    private RectTransform pointerRectTransform;
    public RectTransform minimapRectTransform;
    public Camera minimapCamera;
    public float borderSize = 60f;

    private Image pointerImage;
    public Sprite arrowSprite;
    public Sprite gpsSprite;


    void Awake()
    {
        pointerRectTransform = GetComponent<RectTransform>();
        pointerImage = GetComponent<Image>();
    }

    void Update()
    {
        //The Arrow points to Actor from the Player.
        this.trTarget = GameManager.instance.trNextTarget;
        if (trPlayer == null || trTarget == null)
        {
            Debug.Log("trNextTarget in GameManager is null, so no Update function!!!");
            pointerImage.sprite = null;
            //안 보이게 멀치감치 떨어뜨려 놓자...null이라도 하얗게 나오는구나...
            pointerRectTransform.anchoredPosition = new Vector2(Screen.width, Screen.height);
            return;
        }

        Vector3 positionTargetInViewPort = minimapCamera.WorldToViewportPoint(trTarget.position);

        float borderSizeWidthInViewPort = this.borderSize / minimapRectTransform.rect.width;
        float borderSizeHeightInViewPort = this.borderSize / minimapRectTransform.rect.height;

        bool isOffScreen = positionTargetInViewPort.x <= borderSizeWidthInViewPort || positionTargetInViewPort.x >= 1 - borderSizeWidthInViewPort
            || positionTargetInViewPort.y <= borderSizeHeightInViewPort || positionTargetInViewPort.y >= 1 - borderSizeHeightInViewPort;
        
        Vector3 positionArrowInMinimap = positionTargetInViewPort;
        if (isOffScreen)
        {
            RotatePointerToTarget();

            if (positionArrowInMinimap.x <= borderSizeWidthInViewPort) positionArrowInMinimap.x = borderSizeWidthInViewPort;
            if (positionArrowInMinimap.x >= 1f - borderSizeWidthInViewPort) positionArrowInMinimap.x = 1f - borderSizeWidthInViewPort;
            if (positionArrowInMinimap.y <= borderSizeHeightInViewPort) positionArrowInMinimap.y = borderSizeHeightInViewPort;
            if (positionArrowInMinimap.y >= 1 - borderSizeHeightInViewPort) positionArrowInMinimap.y = 1f - borderSizeHeightInViewPort;

            pointerImage.sprite = arrowSprite;
        }
        else
        {
            //Pause rotate if icon is above the target.
            pointerRectTransform.localEulerAngles = new Vector3(0, 0, 0);
            pointerImage.sprite = gpsSprite;
        }
        
        Vector2 arrowCoordInViewPort = new Vector2(positionArrowInMinimap.x - pointerRectTransform.pivot.x
            , positionArrowInMinimap.y - pointerRectTransform.pivot.y);

        arrowCoordInViewPort.x = arrowCoordInViewPort.x * minimapRectTransform.rect.width;
        arrowCoordInViewPort.y = arrowCoordInViewPort.y * minimapRectTransform.rect.height;
        pointerRectTransform.anchoredPosition = arrowCoordInViewPort;

    }

    void RotatePointerToTarget()
    {
        Vector3 positionTargetInViewPort = minimapCamera.WorldToViewportPoint(trTarget.position);
        Vector3 positionPlayerInViewPort = minimapCamera.WorldToViewportPoint(trPlayer.position);

        Vector3 direction = (positionTargetInViewPort - positionPlayerInViewPort).normalized;

        float angle = UtilsClass.GetAngleFromVectorFloat(direction);
        pointerRectTransform.localEulerAngles = new Vector3(0, 0, angle);
    }


}
