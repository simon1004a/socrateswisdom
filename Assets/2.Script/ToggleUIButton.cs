﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToggleUIButton : MonoBehaviour {

    public Image btnImage;

    public Sprite uiSpriteA;
    public Sprite uiSpriteB;

    bool bBtnOn = false;

	// Use this for initialization
	void Start () {

        btnImage = GetComponent<Image>();

        SetUIBtnImage();


	}

    public void ToggleUIBtnImage()
    {
        bBtnOn = !bBtnOn;

        if (!bBtnOn)
            btnImage.sprite = uiSpriteA;
        else
            btnImage.sprite = uiSpriteB;
    }


    public void SetUIBtnImage()
    {
        bBtnOn = GameManager.instance.bCamZoomMode;


        if (!bBtnOn)
            btnImage.sprite = uiSpriteA;
        else
            btnImage.sprite = uiSpriteB;
    }

}
