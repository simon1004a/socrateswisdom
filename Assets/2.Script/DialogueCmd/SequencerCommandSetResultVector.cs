using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;
using PrimitiveUI.Examples;

namespace PixelCrushers.DialogueSystem.SequencerCommands {

	public class SequencerCommandSetResultVector : SequencerCommand
    { 
		public void Start()
        {            
            //기하학 작도 UI를 켤 때나 끌 때나 일단 다 초기화 하고 시작하자!
            MathManager.instance.SetResultVector(new Vector4(
                GetParameterAsFloat(0),
                GetParameterAsFloat(1),
                GetParameterAsFloat(2),
                GetParameterAsFloat(3))
                );

            Stop();
        }
		
	}

}