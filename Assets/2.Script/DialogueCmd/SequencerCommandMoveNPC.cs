using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;
using UnityEngine.AI;

namespace PixelCrushers.DialogueSystem.SequencerCommands {


    //원래 MoveTo(PointA, Socrates, 3) 이런 형식으로 내재된 함수를 쓸 수 있으나 
    //내가 짜 놓았던 CounselorCtrl이랑 잘 맞지 않아 직접 짜기로 하였다...
    //스크립트에서는 MoveNPC(PointA, Socrates)  이런 형식으로 쓰면 된다. 

    public class SequencerCommandMoveNPC : SequencerCommand {

        Transform target;
        GameObject NPC;

		public void Start()
        {
            string spawnPoint = GetParameterAs<string>(0, "");
            string npcName = GetParameterAs<string>(1,"");            
            Debug.Log("Moving Point is " + spawnPoint + " and NPC is " + npcName);

            if (npcName.Length != 0 && spawnPoint.Length != 0)
            {
                target = GetSubject(spawnPoint);
                NPC = GameObject.Find(npcName);
            }

            Stop();

		}

        //대화에서 Continue 버튼을 누르는 순간 이 함수가 불리게 된다. 
        //그러니깐 이 Sequence를 포함하는 Dialogue가 종료될 때에 이 함수가 불린다. 
        //그 때 명령어를 실행하는 게 타이밍이 적당한 듯 하다.
        public void OnDestroy()
        {
            if (NPC != null && target != null)
            {
                var nav = NPC.GetComponent<NavMeshAgent>();
                var ctrl = NPC.GetComponent<CounserlorCtrl>();

                nav.SetDestination(target.position);
                ctrl.CurrentMode = CounserlorCtrl.NpcMode.MOVING_MODE;
            }
		}
		
	}

}
 

/**/