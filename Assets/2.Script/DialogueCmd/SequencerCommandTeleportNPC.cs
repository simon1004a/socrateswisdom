using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;

namespace PixelCrushers.DialogueSystem.SequencerCommands {

    //MoveNPC(PointA, Socrates)에서 카피해와서 만든 명령어. 단순 Teleport이기에
    //Nav Mesh 기능을 빼고 매우 쉽게 그냥 위치 좌표로 바로 점프하게 짰다. 
    //Angel이든 Counselor이든 모두 쉽게 쓸 수 있다. 

    public class SequencerCommandTeleportNPC : SequencerCommand {

        Transform target;
        GameObject NPC;

		public void Start()
        {
            string spawnPoint = GetParameterAs<string>(0, "");
            string npcName = GetParameterAs<string>(1,"");
            Debug.Log("Teleport Point is " + spawnPoint + " and NPC is " + npcName);

            if (npcName.Length != 0 && spawnPoint.Length != 0)
            {
                target = GetSubject(spawnPoint);
                NPC = GameObject.Find(npcName);                
            }
            Stop();
		}


        //대화에서 Continue 버튼을 누르는 순간 이 함수가 불리게 된다. 
        //그러니깐 이 Sequence를 포함하는 Dialogue가 종료될 때에 이 함수가 불린다. 
        //그 때 명령어를 실행하는 게 타이밍이 적당한 듯 하다. 
		public void OnDestroy()
        {
            if (NPC != null && target != null)
            {                
                NPC.transform.position = new Vector3(target.position.x, 0, target.position.z);
            }
        }
		
	}

}
 

/**/