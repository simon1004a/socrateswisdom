using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;
using PrimitiveUI.Examples;

namespace PixelCrushers.DialogueSystem.SequencerCommands {

	public class SequencerCommandDrawLine : SequencerCommand
    { 
		public void Start()
        {
            Vector2 pt1 = Vector2.zero;
            Vector2 pt2 = Vector2.zero;

            pt1.x = GetParameterAsFloat(0);
            pt1.y = GetParameterAsFloat(1);

            pt2.x = GetParameterAsFloat(2);
            pt2.y = GetParameterAsFloat(3);

            Debug.Log("Cmd DrawLine called!!!" + pt1 + " and " + pt2);
            //기하학 작도 UI를 켤 때나 끌 때나 일단 다 초기화 하고 시작하자!
            MathManager.instance.DrawLine(pt1, pt2);

            Stop();
        }
		
	}

}