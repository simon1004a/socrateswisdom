using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;
using PrimitiveUI.Examples;

namespace PixelCrushers.DialogueSystem.SequencerCommands {

	public class SequencerCommandEnableMath : SequencerCommand
    { 
		public void Start()
        {
            var bCanvas = GetParameterAsBool(0);
            Debug.Log("Cmd EnableMath called!!!" + bCanvas);
            //기하학 작도 UI를 켤 때나 끌 때나 일단 다 초기화 하고 시작하자!
            MathManager.instance.ClearFigures();
            MathManager.instance.EnableMath(bCanvas);

            //MathCanvas가 켜지는 순간이 곧 기하학 문제 모드로 들어가는 순간이다. 
            if(bCanvas)
                GameManager.instance.ChangeGameMode(GameManager.GameMode.MATH_MODE);
            
            Stop();
        }
		
	}

}