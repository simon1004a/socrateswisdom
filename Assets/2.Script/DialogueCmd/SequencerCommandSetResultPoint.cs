using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;
using PrimitiveUI.Examples;

namespace PixelCrushers.DialogueSystem.SequencerCommands {

	public class SequencerCommandSetResultPoint : SequencerCommand
    { 
		public void Start()
        {            
            //기하학 작도에 성공했을 경우 받을 point 지정해주기!
            MathManager.instance.SetResultPoint( GetParameterAsInt(0));
            Stop();
        }
		
	}

}