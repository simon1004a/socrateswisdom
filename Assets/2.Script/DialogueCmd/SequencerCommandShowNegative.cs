﻿using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;

namespace PixelCrushers.DialogueSystem.SequencerCommands
{
    public class SequencerCommandShowNegative : SequencerCommand
    {
        Transform trPlayer;
        Transform trConversant;

        public void Start()
        {
            trPlayer = GameManager.instance.GetPlayerTransform();
            trConversant = GameManager.instance.GetConversantTransform();
     
            GameManager.instance.PlayLoss();
            trPlayer.GetComponent<Animator>().SetTrigger("Bad");
            if (trConversant != null)
                trConversant.GetComponent<Animator>().SetTrigger("Negative");
        }

        //대화에서 Continue 버튼을 누르는 순간 이 함수가 불리게 된다. 
        //그러니깐 이 Sequence를 포함하는 Dialogue가 종료될 때에 이 함수가 불린다. 
        public void OnDestroy()
        {
            //GoogleAdManager.instance.ShowInterstitial();
        }

    }

}
