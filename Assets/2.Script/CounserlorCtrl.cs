﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CounserlorCtrl : MonoBehaviour {

    public enum NpcMode
    {
        WANDER_MODE,
        MOVING_MODE,
    }

    public NpcMode CurrentMode = NpcMode.WANDER_MODE;

    private Animator animator;

    public AudioClip meetCounselor1;
    public AudioClip meetCounselor2;
    public AudioClip meetCounselor3;

    //상담자의 배회 기능.
    private bool IsCoroutineOn = false;
    private IEnumerator coroutine = null;    
    public Vector3 AnchorPoint = Vector3.zero;
    private Vector3 RandomPoint = Vector3.zero;

    public Vector2 AIMoveRange = new Vector2(0.0f,3.0f);        //인공지능에서, 어느 정도의 거리를 왔다 갔다 거릴지 결정하는 변수
    public Vector2 AIMoveTime = new Vector2(10.0f, 15.0f);   //인공지능에서 몇 초마다 움직일 것인지 결정하는 변수          

    //Nav 기능
    private NavMeshAgent nvAgent;
    private Transform player;
    bool IsplayerNear = false;

    //보일 때만 움직이도록 Renderer가져오기
    public Renderer airender;    
    const float AI_ACTIVATE_RANGE = 15.0f;


    // Use this for initialization
    void Start ()
    {
        animator = GetComponent<Animator>();        
        nvAgent = GetComponent<NavMeshAgent>();
        nvAgent.enabled = false;
        airender = GetComponentsInChildren<Renderer>()[0];
        player = GameObject.FindGameObjectWithTag("Player").transform;

        coroutine = Wander();

    }
    
    IEnumerator Wander()
    {        
        while (true)
        {
            //적당히 기다리다가
            yield return new WaitForSeconds(Random.Range(AIMoveTime.x, AIMoveTime.y));

            //Debug.Log(name + "Coroutine is Working");

            if (!IsplayerNear && CurrentMode == NpcMode.WANDER_MODE)
            {
                // 저장된 position data의 경우 Start 함수가 불린 후 몇 프레임이 지난 뒤에 불리기에, 
                // Start함수가 불릴 때의 캐릭터 위치 좌표는 저장한 것과 달리 원래 Scene의 배치 좌표이다. 
                //그래서 AnchorPoint init을 Start에서 여기로 옮겼다.........
                AnchorPoint = transform.position;
                //임의의 지점으로 그냥 걸어가도록 하자. 새로운 목표 지점을 정해진 범위 내에서 random하게 결정한다.                
                Vector2 randCoord = Random.insideUnitCircle * Random.Range(AIMoveRange.x, AIMoveRange.y);
                RandomPoint.Set(randCoord.x, 0, randCoord.y);
                RandomPoint += AnchorPoint;
                //Debug.Log( name + "is moving to " + RandomPoint);
                nvAgent.enabled = true;
                nvAgent.SetDestination(RandomPoint);
            }

            if (CurrentMode == NpcMode.MOVING_MODE)
            {
                AnchorPoint = nvAgent.destination;
                if (Vector3.Distance(transform.position, nvAgent.destination) < 0.1f)
                {
                    CurrentMode = NpcMode.WANDER_MODE;                    
                }
            }
        }
    }

    private void Update()
    {        
        if (IsAIAlive())
        {
            if (!IsCoroutineOn)
            {                
                StartCoroutine(coroutine);
                //Debug.Log(name + "Coroutine Started!");
                IsCoroutineOn = true;
            }
            
            animator.SetFloat("Speed", nvAgent.velocity.magnitude);
            
        }
        else
        {
            if (IsCoroutineOn)
            {
                StopCoroutine(coroutine);
                //Debug.Log(name + "Coroutine finished!");
                IsCoroutineOn = false;
                animator.SetFloat("Speed", 0.0f);
            }            
        }
        
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            //Debug.Log("Player is near!");

            IsplayerNear = true;
            transform.LookAt(player);
            nvAgent.enabled = true;

            GameManager.instance.RandomizeSfx(meetCounselor1, meetCounselor2, meetCounselor3);

            //플레이어에게로 향하되 플레이어와 NPC의 중간 지점으로 접근(부딪히지 않게)
            Vector3 destination = (player.position + transform.position)/2;
            nvAgent.SetDestination(destination);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            transform.LookAt(player);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            //Debug.Log("Player has gone");
            IsplayerNear = false;
        }
    }


    //AI가 움직일 수 있는 조건 체크.
    public bool IsAIAlive()
    {
        //일단 거리가 한계 이상이면 움직이지 않는다.
        float distFromPlayer = Vector3.Distance(transform.position, player.position);
        if (distFromPlayer > AI_ACTIVATE_RANGE)
        {
            return false;
        }
        else if (distFromPlayer < 1.5f) //너무 짧아도 Coroutine을 가동하지 않도록 하자...
        {
            return false;
        }
        
        //카메라에 보이면 true이되 아니면 false이다. renderer가 없는 경우 그냥 안 움직이게 한다. 
        return (airender != null) ? airender.isVisible : false;
    }

}
