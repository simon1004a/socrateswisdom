﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace PixelCrushers.DialogueSystem
{

    /// <summary>
    /// Add this to the Dialogue Manager to allow it to dispatch quest state updates
    /// to QuestStateListener components on other GameObjects.
    /// </summary>
#if UNITY_5_1 || UNITY_5_2 || UNITY_5_3_OR_NEWER
    [HelpURL("http://pixelcrushers.com/dialogue_system/manual/html/quest_log_window.html#questIndicator")]
#endif
    [AddComponentMenu("Dialogue System/Miscellaneous/Quest Indicators/Quest State Dispatcher (on Dialogue Manager)")]
    public class QuestStateDispatcher : MonoBehaviour
    {

        private List<QuestStateListener> m_listeners = new List<QuestStateListener>();

        public void AddListener(QuestStateListener listener)
        {
            if (listener == null) return;
            m_listeners.Add(listener);
        }

        public void RemoveListener(QuestStateListener listener)
        {
            m_listeners.Remove(listener);
        }

        public void OnQuestStateChange(string questName)
        {
            Debug.Log("OnQuestStateChange : " + questName);
            
            //TODO : 질문 Tree를 키우도록 해봅시다!
            //현재 진행되는 Quest를 변수로 두고 Quest가 해결되었는지, 
            //또 새로운 Quest가 할당되면 나무가 계속 자라는 식으로 - 해도 좋습니다.
            
            //유의할 점은, Save/Load 과정에 이 질문 Tree나 GPS 타겟도 자연스레 저장이 되어야 한다는 것입니다!
            for (int i = 0; i < m_listeners.Count; i++)
            {
                var listener = m_listeners[i];
                if (string.Equals(questName, listener.questName))
                {
                    listener.OnChange();
                }
            }

            //새로운 Quest에 적합하도록 GPS의 Target을 설정해줍니다. Target이 여러개라면 가장 가까운 쪽으로 설정합니다. 
            GameManager.instance.SetNextTargetCounselor();
        }

    }
}