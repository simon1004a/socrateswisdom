using UnityEngine;

namespace PixelCrushers.DialogueSystem.Examples {

	/// <summary>
	/// Component that keeps its game object always facing the main camera.
	/// </summary>
	[AddComponentMenu("Dialogue System/Actor/Always Face Camera")]
	public class AlwaysFaceCamera : MonoBehaviour {
		
		public bool yAxisOnly = false;
		
		private Transform myTransform = null;

        //For minimap mode
        public Transform trMinimapCamera;
        public bool isCameraMinimapMode = false;
        const float questIndicatorOriginalScale = 0.005f;

        private RectTransform rectTransform = null;
		void Awake()
        {
			myTransform = transform;
            try { trMinimapCamera = GameObject.Find("MinimapCamera").transform; }
            catch { Debug.Log("Failed to find MinimapCamera!!!"); trMinimapCamera = null; }

            rectTransform = GetComponent<RectTransform>();
		}
	
		void Update()
        {
            if (myTransform != null)
            {
                if ( trMinimapCamera != null && GameManager.instance.CurrentMode == GameManager.GameMode.MINIMAP_MODE )
                {
                    myTransform.LookAt(trMinimapCamera);
                    if (!isCameraMinimapMode)
                    {
                        //The moment camera changed to minimap mode
                        isCameraMinimapMode = true;
                        rectTransform.localScale = new Vector3(questIndicatorOriginalScale * -10f, questIndicatorOriginalScale * 10f, 1f);
                    }
                    return;
                }

                if (isCameraMinimapMode)
                {
                    //The moment camera changed to main cam
                    isCameraMinimapMode = false;
                    rectTransform.localScale = new Vector3(questIndicatorOriginalScale * -1f, questIndicatorOriginalScale, 1f);
                }

                if (UnityEngine.Camera.main != null)
                {
                    var trMainCamera = Camera.main.transform;

                    if (yAxisOnly)
                        myTransform.LookAt(new Vector3(trMainCamera.position.x, myTransform.position.y, trMainCamera.position.z));
                    else
                        myTransform.LookAt(trMainCamera);
                }
            }                
		}
		
	}

}
